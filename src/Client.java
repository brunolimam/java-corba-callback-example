import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import Callback.CallbackIn;
import Callback.CallbackInHelper;
import Callback.CallbackOut;
import Callback.CallbackOutHelper;


public class Client {
	public static void main(String[] args) {
    	try{
            //initialize orb
    		ORB orb = ORB.init(args,null);
    		
    	 	 //Instantiate Servant and create reference
            POA rootPOA = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
            CallbackOutImpl callbackOutImpl = new CallbackOutImpl();
            rootPOA.activate_object(callbackOutImpl);
            CallbackOut ref = CallbackOutHelper.narrow(rootPOA.servant_to_reference(callbackOutImpl));
            
            //Resolve MessageServer
            org.omg.CORBA.Object obj = orb.resolve_initial_references("NameService");
    	 	NamingContext naming = NamingContextHelper.narrow(obj);
            NameComponent[] name = {new NameComponent("Callback","Exemplo")};
    		org.omg.CORBA.Object objRef =  naming.resolve(name);
    		CallbackIn callbackIn = CallbackInHelper.narrow(objRef);
    		
            //Register listener reference (callback object)
    		callbackIn.register(ref);
    		
          	rootPOA.the_POAManager().activate();

            //Wait for messages
            System.out.println("Wait for incoming messages");
            orb.run();

    	}
    	catch (Exception ex){
    		System.out.println("Erro");
    		ex.printStackTrace();
    	}
    }
}
