import Callback.CallbackInPOA;
import Callback.CallbackOut;

public class CallbackInImpl extends CallbackInPOA {
		
	@Override
	public void register(CallbackOut lt) {
		// TODO Auto-generated method stub
		System.out.println("Hello world do servidor");
		
		Thread callbackThread = new Thread() {
			public void run() {
				lt.callback("Isso é uma mensagem de retorno, hello world!");
			};
		};
		
		callbackThread.start();
	}
}